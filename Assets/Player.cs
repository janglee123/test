using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private float speed = 5.0f;
    private Vector2 inputDir;
    private Rigidbody2D rb;
    private float previousDir = 1.0f;
    public Transform legCollider;
    private float legColliderRadius = 0.25f;
    private bool isOnGround = true;

    private float jumpForce = 5.0f;
    public LayerMask groundMask;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        isOnGround = Physics2D.OverlapCircle(legCollider.position, legColliderRadius, groundMask);
        inputDir.x = Input.GetAxisRaw("Horizontal");
        inputDir.y = Input.GetAxisRaw("Vertical");

        if(inputDir.x != previousDir && inputDir.x != 0.0f){
            Flip();
            previousDir = inputDir.x;
        }


        if(isOnGround && inputDir.y > 0.0f)
        {        
            Jump();
        }
    }

    void FixedUpdate()
    {
        if(isOnGround)
        {
            rb.velocity = new Vector2(speed * inputDir.x, rb.velocity.y);
        }
    }

    void Flip()
    {
        Vector3 scale = transform.localScale;
        scale.x *= - 1;
        transform.localScale = scale;        
    }

    void Jump(){
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
    }
}
